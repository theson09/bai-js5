/* input Điểm Chuẩn: 27
        Khu vực A
        Đối tượng 1
        Điểm 1: 10
        Điểm 2: 9
        Điểm 3: 9
    => output: Bạn đã đậu, số điểm của bạn là 30 */
document.getElementById("btn-1").onclick=function(){
    var diemChuan = document.getElementById("txt-diem-chuan").value*1;
    var khuVuc = document.getElementById("txt-khu-vuc").value;
    var doiTuong = document.getElementById("txt-doi-tuong").value;
    var diem1 = document.getElementById("txt-diem1").value*1;
    var diem2 = document.getElementById("txt-diem2").value*1;
    var diem3 = document.getElementById("txt-diem3").value*1;
var diemKhuVuc = khuVuc == "A" ? 2: khuVuc == 'B' ? 1: khuVuc == 'C' ? 0.5 : 0
var diemDoiTuong = doiTuong === 1 ? 2.5: doiTuong === 2 ? 1.5: doiTuong === 3 ? 1 : 0
var diemTong = diem1 + diem2 + diem3 + diemKhuVuc + diemDoiTuong;
if(diemTong >= diemChuan){
    document.getElementById("ketQua-1").innerHTML=`Bạn đã đậu, số điểm của bạn là ${diemTong}`;
}
else{
    document.getElementById("ketQua-1").innerHTML=`Bạn đã rớt, số điểm của bạn là ${diemTong}`;
}
if(diem1<=0 || diem2<=0 || diem3<=0){
    document.getElementById("ketQua-1").innerHTML=`Bạn đã rớt do điểm có điểm liệt, mẹ biết mẹ buồn đó !!!`
}
}
// Bài 2
// input: Tên: Truong, Số kw: 70kw
// output: Tên: Truong, Số tiền: 38,000 VND
document.getElementById('btn-2').onclick=function(){
    var name=document.getElementById('name').value;
    var soKw=document.getElementById('kw').value*1;
    var soTien;

    if(soKw <= 0){
        document.getElementById('ketQua-2').innerHTML=`Số Kw không hợp lệ !!!`;
    }
    // 50kw đầu
    else if (soKw <= 50){
        soTien = soKw*500;
    }
    // 50 kw kế
    else if (soKw <= 100){
        soTien = (50*500)+(soKw-50)*650;
    }
    // 100 kw kế
    else if (soKw <= 200){
        soTien = (50*500)+(50*650)+(soKw-100)*850;}
    // 150 kw kế
    else if (soKw <= 350){
        soTien = (50*500)+(50*650)+(50*850)+(soKw-200)*1100;
    }
    // số kw còn lại
    else {
        soTien = (50*500)+(50*650)+(50*850)+(50*1100)+(soKw-350)*1300;
    }
    document.getElementById('ketQua-2').innerHTML=`Tên chủ nhà: ${name}, Số tiền điện phải trả: ${soTien.toLocaleString( )} VND`;
    }
